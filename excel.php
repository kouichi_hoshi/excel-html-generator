<?php

// Excelシート番号
define('XLS_META',       0); // 今回使うシート
define('XLS_SNS',        1);
define('XLS_ITEM_LIST',  2);


// 全体の配列を取得する
$metaData = EXCEL::getXls(dirname(__FILE__). '/dh.xlsx', XLS_META);
// echo '<pre>';
// var_dump($metaData);
// echo '</pre>';
$img  = 'images/';
$flag = true;

foreach ($metaData as $key => $value) {

    $book = $value['雑誌'];
    $pege = $value['ページ'];
    $photo = $value['写真'];

    if($pege){
        echo '</div>';
    }

    //雑誌情報
    if($book){

        if(!$flag){
            echo '</section>';
        }

        echo '<section class="book">';
        echo '<div class="page" data-id="'.$value['写真'].'">';
        echo '<div class="page_img--head clearfix">';
        echo '<div class="page_img--head__right book_thumbnail">';
        echo '<p><img src="'.$img.$value['カバー'].'"></p>';
        echo '<p class="book_thumbnail__data">';//雑誌表紙
        echo '<strong>'.$value['雑誌'].'</strong>';
        echo $value["号"].'<br />';
        echo $value["発売日"].'発売';
        echo '</p>';
        echo '</div>';

        echo '<div class="page_img--head__left">';
        echo '<a class="swipe" rel="group1" href="'.$img.$value['写真'].'" title="">';//拡大用画像src
        echo '<img src="'.$img.$value['写真'].'" alt="" />';
        echo '</a>';
        echo '</div>';
        echo '</div><!-- page_img--head__left end -->';

    } else {

        if($pege){
            echo '<div class="page" data-id="'.$value['写真'].'">';      
        }

        if($photo){
            echo '<div class="page_img">';
            echo '<a class="swipe" rel="group1" href="'.$img.$value['写真'].'" />';
            echo '<img src="'.$img.$value['写真'].'" alt="" />';
            echo '</a>';
            echo '</div><!-- page_img end -->';            
        }

    }

    //商品情報
    if($pege){
        echo '<div>'.$pege.'</div>';
    }

    echo '<div>';
    echo '<a href="'.$value["リンク"].'">';
    echo '<span class="tdul">'.$value["商品名"].'</span>';
    echo $value["商品名"];
    echo $value["色"];
    echo $value["価格（税込）"];
    echo '</a>';
    echo '</div>';

    $flag = false;           

}


// ----------------- Excel操作クラス
/**
 *
 * @author  wilf312 <wilf33312@gmail.com>
 * @create  2013/05/30
 * @version v 0.5 2013/05/30 02:17:19 Okada
 **/

Class Excel {
    public static function getXls($aReadFilePath, $aSheetNum)
    {

        // ライブラリファイルの読み込み
        require_once dirname(__FILE__). "/PHPExcel/IOFactory.php";


        // ファイルの存在チェック
        if (!file_exists($aReadFilePath)) {
            exit($aReadFilePath. "が見つかりません。" . EOL);
        }

        // xlsxをPHPExcelに食わせる
//        $objPExcel = PHPExcel_IOFactory::load($aReadFilePath);
        if($_POST{'mode'} == "upload"){
            //Excel読み込み
            $filepath = $_FILES["upload_file"]["tmp_name"];
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objPExcel = $objReader->load($filepath);
        }

        // 配列形式で返す
        return Excel::getTitleArray($objPExcel->setActiveSheetIndex($aSheetNum)->toArray(null,true,true,true));

    }

    // 1行目を階層2の配列のキーにページの配列を作成
    // 階層2への配列アクセスはSCRIPT_NAMEを使用できるようにURLを配列のキーにする
    public static function getTitleArray($aTableData)
    {

        $titleArray = array();
        $keyData = array();
        $isFirst = true;


        // 配列の整形
        foreach($aTableData as $k => $v) {

            if ($isFirst) {

                // タイトル行を取得
                $keyData = $v;
                $isFirst = false;

            } else {

                $_simpleData = array(); // 仮データ保存配列

                // タイトル行のフィールド名を取得して配列を作成する。
                foreach($keyData as $k2 => $v2) {

                    // IDの場合だけ 値をfloatからintに変換しておく
                    if ($v2 === 'id') {
                        $_simpleData[$v2] = (int)$v[$k2];

                    }else if ($v2 === 'ページid') {
                        $_simpleData[$v2] = (int)$v[$k2];

                    } else {
                        $_simpleData[$v2] = $v[$k2];
                    } 

                }

                // 作成した配列を追加
                $titleArray[$_simpleData['id']] = $_simpleData;
            }
        }

        return $titleArray;
    }
}


$heredocs = <<< EOM

<style>


    .item{
            width: 100% !important;
    }

    .book{
        margin-bottom: 170px;
    }

    .page{
        margin-bottom: 60px;
    }

    .page_img{
        margin-bottom: 10px;
    }

    @media screen and (min-width: 768px){
        .page_img{
            width: 80%;
        }
    }

    .page_img img{
        margin-bottom: 0 !important;
    }

    .page_img--head{

    }

    @media screen and (min-width: 768px){

        .page_img--head .page_img--head__right{
            float: right;
            width: 15%;
        }

        .page_img--head .page_img--head__left{
            float: left;
            width: 80%;
            margin-right: 5%;
        }

    }

    .book_thumbnail{
        max-width: 280px;
        margin: 0 auto 20px auto;
        text-align: center;
    }

    @media screen and (min-width: 768px){
        .book_thumbnail{
            text-align: left;
        }
    }

    .book_thumbnail img{
        margin-bottom: 5px !important;
    }

    .book_thumbnail__data strong{
        display: block;
        font-weight: bold;
        letter-spacing: 0.5px;
        font-size: 14px;
        line-height: 15px !important;
    }

    dl.page_data dt{
        font-weight: normal;
    }

    dl.page_data dd{
        margin-left: 0;
        margin-bottom: 5px;
    }

    dl.page_data a {
        color: #454545;
        display: inline !important;
    }

    dl.page_data a:hover {
        color: #9e9e9e !important;
        opacity: 0.9;
    }

    dl.page_data .tdul {
        text-decoration: underline;
    }

</style>

<!-- CSSの読み込み -->
<link rel="stylesheet" href="http://ds-assets.s3.amazonaws.com/denham/news/zassi/js/photoswipe/photoswipe.css">
<script src="http://ds-assets.s3.amazonaws.com/denham/news/zassi/js/jquery-1.10.2.min.js"></script>
<script src="http://ds-assets.s3.amazonaws.com/denham/news/zassi/js/photoswipe/jquery.photoswipe.js"></script>
<script>
    (function($){
        $(function(){
            $('a.swipe').photoSwipe();
        });
    })(jQuery);
</script>


EOM;

echo $heredocs;






?>


