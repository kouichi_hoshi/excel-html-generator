<?php

// ファイル名の指定
$readFile = "dh.xlsx";

// 連想配列でデータ受け取り
$data = readXlsx($readFile);

// ファイル名渡したら配列返すラッパー関数
function readXlsx($readFile)
{
    // ライブラリファイルの読み込み （パス指定し直す）
    require_once dirname(__FILE__) . '/PHPExcel/IOFactory.php';

    // ファイルの存在チェック
    if (!file_exists($readFile)) {
        exit($readFile. "が見つかりません。" . EOL);
    }

    // xlsxをPHPExcelに食わせる
    $objPExcel = PHPExcel_IOFactory::load($readFile);

    // 配列形式で返す
    $data = $objPExcel->getActiveSheet()->toArray(null,true,true,true);

    // $arr = array();

    // foreach ($data as $row => $arrCol) {
    //     foreach ($arrCol as $col => $val) {
    //         $arr[$col][$row] = $val;
    //     }
    // }
    // return $arr;

    return $data;


}

    foreach ($data as $key => $value) {
//        var_dump($value);
        foreach ($value as $key2 => $value2) {
            echo '<p>';
            if(isset($value2)){
                echo $key2.':';
            }
            echo $value2;
            echo '</p>';
        }
    }

// 出力確認
// print '<pre>';
// var_dump($data);
// print '</pre>';


// foreach ($data as $col => $arrRows) {
//     echo $col;
//     echo $arrRows['1'];
//     echo $arrRows['2'];
//     echo $arrRows['3'];
// }


 // toArrayの説明
/**
 * Create array from worksheet
 *
 * @param mixed $nullValue Value returned in the array entry if a cell doesn't exist
 * @param boolean $calculateFormulas Should formulas be calculated?
 * @param boolean $formatData  Should formatting be applied to cell values?
 * @param boolean $returnCellRef False - Return a simple array of rows and columns indexed by number counting from zero
 *                               True - Return rows and columns indexed by their actual row and column IDs
 * @return array
 */

?>